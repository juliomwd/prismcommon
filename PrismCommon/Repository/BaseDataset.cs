﻿using PrismCommon.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismCommon.Repository
{
    public class BaseDataset
    {
        private static List<User> Users
        {
            get; set;
        } = new List<User>();


        public static List<User> GetUserList()
        {
            if (Users.Count == 0)
            {
                Users.Add(new User()
                {
                    Name = "Julio",
                    Email = "julio.cesar@assert.ifpb.edu.br",
                    Id = 1
                });
                Users.Add(new User()
                {
                    Name = "Julio2",
                    Email = "julio2.cesar@assert.ifpb.edu.br",
                    Id = 2
                });

                Users.Add(new User()
                {
                    Name = "Julio3",
                    Email = "juli3o.cesar@assert.ifpb.edu.br",
                    Id = 3
                });

                Users.Add(new User()
                {
                    Name = "Julio4",
                    Email = "julio4.cesar@assert.ifpb.edu.br",
                    Id = 4
                });
            }
            return Users;
        }

        public static void AddUser(User user)
        {
            user.Id = Users.Count + 1;
            Users.Add(user);
        }

    }
}
