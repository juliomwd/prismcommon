﻿using PrismCommon.Views;
using Prism.Ioc;
using Prism.Modularity;
using System.Windows;
using Prism.Unity;

namespace PrismCommon
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<Page1>("Page1");
            containerRegistry.RegisterForNavigation<Page2>("Page2");
        }
    }
}
