﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using PrismCommon.Model;
using PrismCommon.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace PrismCommon.ViewModels
{
    public class Page2ViewModel : BindableBase
    {
        private User _user;
        public User User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private DelegateCommand _saveCommand;
        public DelegateCommand SaveCommand =>
            _saveCommand ?? (_saveCommand = new DelegateCommand(ExecuteSaveCommand, CanExecuteSaveCommand)
            .ObservesProperty(() => User.Name)
            .ObservesProperty(() => User.Email));


        public InteractionRequest<INotification> NotificationRequest { get; set; }
        public DelegateCommand NotificationCommand { get; set; }

        void ExecuteSaveCommand()
        {
            BaseDataset.AddUser(User);
            RaiseNotification();
            User = new User();
        }

        bool CanExecuteSaveCommand()
        {
            if (String.IsNullOrWhiteSpace(User.Name) || String.IsNullOrWhiteSpace(User.Email))
            {
                return false;
            }
            return true;
        }

        void RaiseNotification()
        {
            NotificationRequest.Raise(new Notification { Content = "User has saved successfully", Title = "Ok" });
        }


        public Page2ViewModel()
        {
            User = new User();
            NotificationRequest = new InteractionRequest<INotification>();
            NotificationCommand = new DelegateCommand(RaiseNotification);
        }
    }
}
