﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using PrismCommon.Model;
using PrismCommon.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace PrismCommon.ViewModels
{
    public class Page1ViewModel : BindableBase, INavigationAware
    {

        private User _selectedUser;
        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                SetProperty(ref _selectedUser, value);
                RaisePropertyChanged("IsSelected");
            }
        }

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set { SetProperty(ref _users, value); }
        }

        public Visibility IsSelected
        {
            get { return (SelectedUser != null) ? Visibility.Visible : Visibility.Hidden; }

        }

        public Page1ViewModel()
        {
            Users = new ObservableCollection<User>(BaseDataset.GetUserList());
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            //var parameters = navigationContext.Parameters["users"];
            //if(parameters != null)
            //{
            //    Users = parameters as ObservableCollection<User>;
            //}
        }
    }
}
