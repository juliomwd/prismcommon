﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using PrismCommon.Model;
using PrismCommon.Views;
using System.Collections.ObjectModel;
using Unity;

namespace PrismCommon.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "Prism Application";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private IUnityContainer _container { get; set; }
        private IRegionManager _regionManager { get; set; }

        public DelegateCommand<string> NavigateToPage { get; set; }


        private bool CanExecuteNavigate(string parameter)
        {
            return true;
        }

        private void Navigate(string parameter)
        {
            _regionManager.RequestNavigate("ContentRegion", parameter);
        }

        public MainWindowViewModel(IRegionManager regionManager, IUnityContainer container)
        {
            _regionManager = regionManager;
            NavigateToPage = new DelegateCommand<string>(Navigate, CanExecuteNavigate);
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(Page1));
        }
    }
}
