# Prism Sample Application

## Intro

Esta é uma aplicação WPF, utilizando o framework MVVM Prism.

A estrutura está dividida em :
- **Model** - Arquivos com a lógica de negócio;

- **Repository** - Arquivos Responsáveis pela obtenção de dados da aplicação (atualmente com Mock);

- **Utils** - Classes com funcionalidades compartilhadas em toda a aplicação;

- **ViewModels** - Classes responsáveis pelo controle da aplicação, responsável por todo o fluxo de execução da aplicação;

- **Views** - Classes responsáveis pela camada de apresentação da aplicação.

## Extensões
Para a construção desssa aplicação foram utilizadas as seguintes extensões:

- **[Prism Template Pack](http://prismlibrary.github.io/docs/getting-started/productivity-tools.html)** - Extensão com templates predefinidos para a inicialização da aplicação ou de alguns componentes, bem como a adição de um conjunto de snippets para o aumento da produtividade
- **[XAML Styler](https://github.com/Xavalon/XamlStyler/)** - Extensão responsável por formatar os arquivos XAML, organizando-os de uma maneira a qual se prioriza a importância do atributo da Tag

## O Básico

Ao Iniciar uma aplicação Prism utilizando o template base fornecido pela extensão, é gerado um arquivo App.xaml e App.xaml.cs modificado, alterando a composição da classe principal para ser extendida de PrismApplication;

O assistente de criação de aplicações Prism possui por padrão a opção de escolha entre duas bibliotecas de Container, para a injeção de dependência, **DryIoc** e **Unity**, onde a primeira é uma opção mais nova, e a segunda uma mais estável e consolidada.

Nesta aplicação de exemplo, está sendo utilizado o **Unity**
